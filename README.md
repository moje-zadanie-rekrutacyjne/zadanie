# Instrukcja


pobieramy repozytorium:
### `git@gitlab.com:moje-zadanie-rekrutacyjne/zadanie.git`

## KROK 1 - Uruchomienie micro serwisu do pobierania pogody:
przechodzimy do projektu i wykonujemy polecania w konsoli
### `cd weather-service`
### `npm install`
### `npm start`
micro-service uruchomi się pod adresem: \
http://localhost:5000

api jest dostępne pod adresem : \
http://localhost:5000/swagger/

## KROK 2 - Uruchomienie strony:
uruchamiamy osobną konsolę i wykonujemy polecenia
### `cd page`
### `npm install`
### `npm start`
strona uruchomi się pod adresem:\
http://localhost:3000/

## Uruchomienie storybook:
uruchamiamy osobną konsolę i wykonujemy polecenia
### `npm run storybook`
biblioteka komponentów uruchomi się pod adresem: \
http://localhost:6006/