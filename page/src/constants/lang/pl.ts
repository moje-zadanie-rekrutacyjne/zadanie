export const pl = {
    'Common.Search.Buttons.SearchButton.Label': 'Szukaj',
    'Common.Search.Placeholder': 'Wprowadź nawę miasta ',
    'Weather.Details.Wind.Label': 'Prędkość wiatru: ',
    'Weather.Details.Humidity.Label': 'Wilgotność powietrza: ',
    'Errors.City.NotFound': 'Nie znaleziono miasta',
    'Errors.City.NotEntered': 'Nie wprowadzono nazwy miasta',
};
