export enum units {
    KILOMETERS_PER_HOUR = 'km/h',
    DEGREE_CELSIUS = '°C',
    PERCENT = '%',
}
