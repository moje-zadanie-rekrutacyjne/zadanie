import React from 'react';
import { FormattedMessage } from 'react-intl';

export const weatherErrorMap: {[key: string]: string | React.ReactNode} = {
    404: <FormattedMessage id="Errors.City.NotFound" />,
};

