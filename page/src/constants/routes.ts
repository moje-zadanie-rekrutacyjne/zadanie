import { lazy } from 'react';

export const routers = [
    {
        exact: true,
        path: '/',
        component: lazy(() => import('../views/Home/Home')),
    },
    {
        exact: false,
        path: '/404',
        component: lazy(() => import('../views/NoMatch/NoMatch')),
    }, {
        exact: false,
        path: '/500',
        component: lazy(() => import('../views/NoMatch/NoMatch')),
    },
    {
        exact: false,
        path: '/:city/',
        component: lazy(() => import('../views/Weather/WeatherDetails')),
    },
    {
        exact: false,
        path: '/*',
        component: lazy(() => import('../views/NoMatch/NoMatch')),
    },
];
