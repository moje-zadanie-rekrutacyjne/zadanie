export const theme = {
    colors: {
        primary: '#00a4ff',
        positive: '#48A273',
        negative: '#FF6161',
        secondaryGrey: '#999999',
        secondaryLightGrey: '#666666',
        basic: '#FFFFFF',
        error: '#FF0000',
    },
    font: {
        size: {
            huge: 100,
            xLarge: 45,
            large: 30,
            medium: 15,
            small: 10,
        },
    },
};
