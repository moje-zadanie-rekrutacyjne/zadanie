export type ValidationError = {
    statusCode: string;
    message: string;
};
