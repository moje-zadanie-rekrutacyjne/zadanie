import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import { routers } from './constants/routes';
import { Loader } from './components/Loader/Loader';
import { IntlProvider } from 'react-intl';
import { pl } from './constants/lang/pl';
import { theme } from './constants/theme';
import { ThemeProvider } from 'styled-components';

export const App = (): JSX.Element => (
    <IntlProvider messages={pl} locale="pl" defaultLocale="pl">
        <ThemeProvider theme={theme}>
            <Provider store={store}>
                <Router>
                    <Suspense fallback={<Loader fullScreen={true} />}>
                        <Switch>
                            {routers.map((route, i) => (
                                <Route
                                    key={i}
                                    {...route}
                                />
                            ))}
                        </Switch>
                    </Suspense>
                </Router>
            </Provider>
        </ThemeProvider>
    </IntlProvider>
);
