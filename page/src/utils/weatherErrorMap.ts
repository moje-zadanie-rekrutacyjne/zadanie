import { weatherErrorMap } from '../constants/weatherErrorMap';
import React from 'react';

export const getWeatherByCityNameErrorMessage = (name: string): string | React.ReactNode => {
    if (weatherErrorMap[name]) {
        return weatherErrorMap[name];
    }
    return 'error';
};
