import React from 'react';
import { HomeContainer } from './styled/HomeContainer';
import { Page } from '../../components/Page/Page';
import { SearchWrapper } from '../../components/Search/SearchWrapper';

const Home = (): JSX.Element => (
    <Page>
        <HomeContainer>
            <SearchWrapper/>
        </HomeContainer>
    </Page>
);

export default Home;
