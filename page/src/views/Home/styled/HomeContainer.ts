import styled from 'styled-components';

export const HomeContainer = styled.div`
    padding-top: 200px;
    display: flex;
    justify-content: center;
    text-align: center;
    height: 100%;
}
`;
