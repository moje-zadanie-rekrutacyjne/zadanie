import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { IState } from '../../reducers';
import * as selectors from '../../selectors';
import * as action from '../../actions/weather.actions';
import { WeatherDetailsContainer } from './styled/WeatherDetailsContainer';
import { Weather } from '../../components/Weather/Weather';
import { Page } from '../../components/Page/Page';
import { WeatherTypes } from '../../components/Weather/constants/WeatherTypes';
import { SearchWrapper } from '../../components/Search/SearchWrapper';
import removeAccents from 'remove-accents';
import { useIntl } from 'react-intl';

const WeatherDetails: React.FC = (): JSX.Element => {
    const { city } = useParams<{ city: string }>();
    const history = useHistory();
    const dispatch = useDispatch();
    const isFetching = useSelector((state: IState) => selectors.isFetching(state));
    const isFetched = useSelector((state: IState) => selectors.isFetched(state));
    const weatherData = useSelector((state: IState) => selectors.getWeatherByCityNameData(state));
    const error = useSelector((state: IState) => selectors.getError(state));
    const { formatMessage } = useIntl();
    useEffect((): void => {
        if (!isFetched && !isFetching && city) {
            dispatch(action.fetchWeatherData(city));
        }
    }, [dispatch, isFetched, isFetching, city]);

    const data = weatherData.find((cityWeather) =>
        removeAccents(cityWeather.name.toLowerCase()) === removeAccents(city.toLowerCase()));
    const comparedCities = weatherData.filter((cityWeather) =>
        removeAccents(cityWeather.name.toLowerCase()) !== removeAccents(city.toLowerCase()));

    if (error?.statusCode) {
        history.push(`/${error?.statusCode}/`);
    }
    return (
        <Page>
            <WeatherDetailsContainer>
                <SearchWrapper/>
                {data ? (
                    <div>
                        <Weather
                            iconCode={data.weather[0].icon}
                            cityName={data.name}
                            temp={data.main.temp}
                            description={data.weather[0].description}
                            windSpeed={data.wind.speed}
                            humidity={data.main.humidity}
                            windLabel={formatMessage({ id: 'Weather.Details.Wind.Label' })}
                            humidityLabel={formatMessage({ id: 'Weather.Details.Humidity.Label' })}
                        />
                        {comparedCities.map((comparedCitiesData) => (
                            <Weather
                                key={comparedCitiesData.name}
                                iconCode={comparedCitiesData.weather[0].icon}
                                cityName={comparedCitiesData.name}
                                temp={comparedCitiesData.main.temp}
                                description={comparedCitiesData.weather[0].description}
                                windSpeed={comparedCitiesData.wind.speed}
                                humidity={comparedCitiesData.main.humidity}
                                type={WeatherTypes.Secondary}
                                tempDifference={comparedCitiesData.main.temp - data.main.temp}
                                windSpeedDifference={comparedCitiesData.wind.speed - data.wind.speed}
                                humidityDifference={comparedCitiesData.main.humidity - data.main.humidity}
                                windLabel={formatMessage({ id: 'Weather.Details.Wind.Label' })}
                                humidityLabel={formatMessage({ id: 'Weather.Details.Humidity.Label' })}
                            />))}
                    </div>
                ) : null}
            </WeatherDetailsContainer>
        </Page>
    );
};

export default WeatherDetails;
