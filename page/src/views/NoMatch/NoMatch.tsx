import React from 'react';
import { useSelector } from 'react-redux';
import { IState } from '../../reducers';
import * as selectors from '../../selectors';
import { getWeatherByCityNameErrorMessage } from '../../utils/weatherErrorMap';
import { NoMatchContainer } from './styled/NoMatchContainer';
import { SearchWrapper } from '../../components/Search/SearchWrapper';
import { Page } from '../../components/Page/Page';

const NoMatch = (): JSX.Element => {
    const error = useSelector((state: IState) => selectors.getError(state));

    return (
        <Page>
            <NoMatchContainer>
                <div className="error-code">{error?.statusCode}</div>
                <div>{getWeatherByCityNameErrorMessage(error?.statusCode)}</div>
                <SearchWrapper/>
            </NoMatchContainer>
        </Page>
    );
};

export default NoMatch;
