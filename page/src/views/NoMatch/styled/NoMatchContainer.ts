import styled from 'styled-components';

export const NoMatchContainer = styled.div`
    text-align: center;
    .error-code {
        font-size: ${(props) => props.theme.font.size.huge}px;
        color: ${(props) => props.theme.colors.primary};
    }    
    .search {
        padding-top: 100px;
        padding-bottom: 50px;
    }  
`;
