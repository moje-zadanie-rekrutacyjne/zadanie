import { combineReducers, Reducer } from 'redux';
import { dataReducer, IDataState } from './data.reducer';

export type IState ={
    data: IDataState;
};

function createReducer(): Reducer {
    const combinedReducers: {data(): IDataState} = {
        data: dataReducer,
    };
    return combineReducers(combinedReducers);
}

export default createReducer();
