import { DataActionTypes } from '../constants/actionTypes';
import { DataActions } from '../actions/weather.actions';
import { ValidationError } from '../types/ValidationError';
import { WeatherData } from '../types/WeatherData';

export type IDataState = {
    error: ValidationError ;
    isFetched: boolean;
    isFetching: boolean;
    weatherData: WeatherData[];
};

export const initialState: IDataState = {
    isFetching: false,
    isFetched: false,
    weatherData: [],
    error: {
        statusCode: '',
        message: '',
    },
};

export function dataReducer(state: IDataState = initialState, action?: DataActions): IDataState {
    if (!action) {
        return state;
    }
    switch (action.type) {
        case DataActionTypes.FETCH_DATA:
            return {
                ...state,
                ...initialState,
                isFetching: true,
            };
        case DataActionTypes.FETCH_DATA_SUCCESS:
            return {
                ...state,
                isFetching: false,
                isFetched: true,
                weatherData: action.weatherData,
            };
        case DataActionTypes.FETCH_DATA_FAILURE:
            return {
                ...state,
                isFetching: false,
                isFetched: true,
                error: action.error,
            };
    }
    return state;
}
