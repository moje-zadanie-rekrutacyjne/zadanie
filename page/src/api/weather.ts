import axios, { AxiosResponse } from 'axios';
import { WeatherData } from '../types/WeatherData';

export function getWeatherByCityName(
    localization: string,
): Promise<AxiosResponse<WeatherData[]>> {
    return axios.get(`http://localhost:5000/api/weather/${localization}`);
}

