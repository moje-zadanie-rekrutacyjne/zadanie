import { IState } from '../reducers';
import { ValidationError } from '../types/ValidationError';
import { WeatherData } from '../types/WeatherData';
import { IDataState } from '../reducers/data.reducer';

function getData(state: IState): IDataState {
    return state.data;
}
export function getWeatherByCityNameData(state: IState): WeatherData[] {
    return getData(state).weatherData;
}
export function isFetching(state: IState): boolean {
    return getData(state).isFetching;
}
export function isFetched(state: IState): boolean {
    return getData(state).isFetched;
}
export function getError(state: IState): ValidationError {
    return getData(state).error;
}
