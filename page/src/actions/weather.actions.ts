import { Action, AnyAction } from 'redux';
import { DataActionTypes } from '../constants/actionTypes';
import { ThunkAction } from 'redux-thunk';
import { IDataState } from '../reducers/data.reducer';
import { ValidationError } from '../types/ValidationError';
import { getWeatherByCityName } from '../api/weather';
import { WeatherData } from '../types/WeatherData';

type IFetchData = Action<DataActionTypes.FETCH_DATA>;

export function fetchWeatherData(localization: string): ThunkAction<Promise<void>, IDataState, undefined, AnyAction> {
    return async (dispatch) => {
        dispatch({
            type: DataActionTypes.FETCH_DATA,
        } as IFetchData);

        try {
            const { data } = await getWeatherByCityName(localization);
            dispatch(fetchWeatherDataSuccess(data));
        } catch (error) {
            dispatch(fetchWeatherDataFailure(error.response.data));
        }
    };
}

interface IFetchDataSuccess extends Action<DataActionTypes.FETCH_DATA_SUCCESS> {
    weatherData: WeatherData[];
}

function fetchWeatherDataSuccess(weatherData: WeatherData[]): IFetchDataSuccess {
    return {
        type: DataActionTypes.FETCH_DATA_SUCCESS,
        weatherData,
    };
}

interface IFetchDataFailure extends Action<DataActionTypes.FETCH_DATA_FAILURE> {
    error: ValidationError;
}

function fetchWeatherDataFailure(error: ValidationError): IFetchDataFailure {
    return {
        type: DataActionTypes.FETCH_DATA_FAILURE,
        error,
    };
}

export type DataActions =
    | IFetchData
    | IFetchDataSuccess
    | IFetchDataFailure;
