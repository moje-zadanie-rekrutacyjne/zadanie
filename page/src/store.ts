import { applyMiddleware, createStore, Middleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './reducers';

const PRODUCTION = process.env.NODE_ENV === 'production';

const loggerMiddleware = createLogger({ collapsed: true });
const middlewares: Middleware[] = [thunkMiddleware];

if (!PRODUCTION) {
    middlewares.push(loggerMiddleware);
}

export default createStore(rootReducer, undefined, applyMiddleware(...middlewares));
