import React, { useCallback, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import * as action from '../../actions/weather.actions';
import { Search } from './Search';
import { useIntl } from 'react-intl';

export const SearchWrapper = (): JSX.Element => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { formatMessage } = useIntl();
    const [errorMessage, setErrorMessage] = useState('');

    const handleFocus = useCallback(async (): Promise<void> => {
        setErrorMessage('');
    }, [setErrorMessage]);

    const handleSubmit = useCallback(async (e: React.SyntheticEvent): Promise<void> => {
        e.preventDefault();
        const target = e.target as typeof e.target & {
            city: { value: string };
        };

        if (target.city.value === '') {
            setErrorMessage(formatMessage({ id: 'Errors.City.NotEntered' }));
        } else {
            await dispatch(action.fetchWeatherData(target.city.value));
            history.push(`/${target.city.value}/`);
        }
    }, [history, dispatch, setErrorMessage, formatMessage]);

    return (
        <Search
            className="search"
            buttonLabel={formatMessage({ id: 'Common.Search.Buttons.SearchButton.Label' })}
            placeHolder={formatMessage({ id: 'Common.Search.Placeholder' })}
            onSubmit={handleSubmit}
            errorMessage={errorMessage}
            onFocus={handleFocus}
        />
    );
};
