import React from 'react';
import { SearchContainer } from './styled/SearchContainer';

interface SearchProps {
    onSubmit(e: React.SyntheticEvent): void;
    onFocus(e: React.SyntheticEvent): void;
    buttonLabel: string;
    placeHolder: string;
    className?: string;
    errorMessage: string;
}

export const Search = ({
    onSubmit,
    className = 'search',
    buttonLabel, placeHolder,
    errorMessage,
    onFocus,
}: SearchProps): JSX.Element => (
    <SearchContainer className={className}>
        <form onSubmit={onSubmit}>
            <input
                aria-label="city"
                className="city-input"
                type="text"
                name="city"
                placeholder={placeHolder}
                onFocus={onFocus}
            />
            {errorMessage ? <div className="error-message">{errorMessage}</div> : null}
            <div>
                <input aria-label="submit" className="submit" type="submit" value={buttonLabel}/>
            </div>
        </form>
    </SearchContainer>
);
