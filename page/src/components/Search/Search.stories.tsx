import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import '../../index.css';
import { Search } from './Search';
import { theme } from '../../constants/theme';
import { ThemeProvider } from 'styled-components';

export default {
    title: 'Search',
    component: Search,
} as ComponentMeta<typeof Search>;

const Template: ComponentStory<typeof Search> = (args) => (
    <ThemeProvider theme={theme}><Search {...args} /></ThemeProvider>
);

export const Basic = Template.bind({});
Basic.args = {
    onSubmit: (e: React.SyntheticEvent) => e,
    onFocus: (e: React.SyntheticEvent) => e,
    buttonLabel: 'Szukaj',
    placeHolder: 'Wprowadź nazwę miasta',
    className: 'className',
    errorMessage: '',
};
