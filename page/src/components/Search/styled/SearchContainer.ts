import styled from 'styled-components';

export const SearchContainer = styled.div`
    display: flex;
    justify-content: center;
    text-align: center;

    .city-input {
        padding: 10px;
        font-size: ${(props) => props.theme.font.size.medium}px;
        border-width: 1px;
        border-color: ${(props) => props.theme.colors.primary};
        background-color: ${(props) => props.theme.colors.basic};
        color: ${(props) => props.theme.colors.primary};
        border-style: solid;
        border-radius: 8px;
        min-width: 500px;
    }
    .city-input:focus {
        outline:none;        
    }
    .city-input::placeholder {
      color: ${(props) => props.theme.colors.secondaryLightGrey};
    }
    
    .submit {
        margin-top: 20px;
        background:linear-gradient(to bottom, #2dabf9 5%, #0688fa 100%);
        background-color: #2dabf9;
        border-radius: 8px;
        border: none;
        display: inline-block;
        cursor: pointer;
        color: ${(props) => props.theme.colors.basic};
        font-family: Arial;
        font-size: ${(props) => props.theme.font.size.medium}px;
        padding: 9px 23px;
        text-decoration: none;
    }
    .submit:hover {
        background:linear-gradient(to bottom, #0688fa 5%, #2dabf9 100%);
        background-color:#0688fa;
    }
    .submit:active {
        position:relative;
        top:1px;
    }
    
    .error-message {
        text-align: left;
        font-size: ${(props) => props.theme.font.size.small}px;
        color: ${(props) => props.theme.colors.error};
    }
}
`;
