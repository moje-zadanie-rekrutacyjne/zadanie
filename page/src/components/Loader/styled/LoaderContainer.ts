import styled from 'styled-components';

interface LoaderContainerProps {
    fullScreen: boolean;
}

export const LoaderContainer = styled.div<LoaderContainerProps>`
    ${(props: LoaderContainerProps) => {
        if (props.fullScreen) {
            return `
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;            
            `;
        }
    }}      
`;
