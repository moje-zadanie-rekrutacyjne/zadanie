import PulseLoader from 'react-spinners/PulseLoader';
import React from 'react';
import { LoaderContainer } from './styled/LoaderContainer';

interface ILoaderProps {
    fullScreen?: boolean;
}

export const Loader = (
    {
        fullScreen = false,
    }: ILoaderProps): JSX.Element =>
    (
        <LoaderContainer fullScreen={fullScreen}>
            <PulseLoader color="#0181c2" />
        </LoaderContainer>
    );
