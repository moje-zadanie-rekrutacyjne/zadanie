import React from 'react';

import { PageContainer } from './styled/PageContainer';
import { useSelector } from 'react-redux';
import { IState } from '../../reducers';
import * as selectors from '../../selectors';
import { Loader } from '../Loader/Loader';

export const Page: React.FC = ({ children }) => {
    const isFetching = useSelector((state: IState) => selectors.isFetching(state));

    if (isFetching) {
        return (<Loader fullScreen={true}/>);
    }

    return (
        <PageContainer>
            {children}
        </PageContainer>
    );
};
