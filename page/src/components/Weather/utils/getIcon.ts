import { iconsMap } from '../constants/iconsMap';
import { svgIcons } from '../assets/svgIcons';

export const getIcon = (name: string): string => {
    if (iconsMap[name]) {
        return iconsMap[name];
    }
    return svgIcons.sunny;
};
