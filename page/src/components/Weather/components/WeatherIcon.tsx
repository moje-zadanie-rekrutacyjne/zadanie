import React from 'react';

interface WeatherIconProps {
    path: string;
}

export const WeatherIcon = ({ path }: WeatherIconProps): JSX.Element => (
    <svg
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        width={120}
        height={120}
        viewBox="0 -5 35 40"
    >
        <path d={path}/>
    </svg>
);

