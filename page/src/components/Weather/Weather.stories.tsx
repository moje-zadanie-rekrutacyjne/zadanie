import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import '../../index.css';
import { Weather } from './Weather';
import { theme } from '../../constants/theme';
import { ThemeProvider } from 'styled-components';
import { WeatherTypes } from './constants/WeatherTypes';

export default {
    title: 'Weather',
    component: Weather,
} as ComponentMeta<typeof Weather>;

const Template: ComponentStory<typeof Weather> = (args) => (
    <ThemeProvider theme={theme}><Weather {...args} /></ThemeProvider>
);

export const Primary = Template.bind({});
Primary.args = {
    iconCode: '',
    cityName: 'Gdańsk',
    temp: 30,
    description: 'Słońce',
    windSpeed: 3,
    humidity: 60,
    type: WeatherTypes.Primary,
    windLabel: 'Prędkość wiatru: ',
    humidityLabel: 'Wilgotność: ',
};

export const Secondary = Template.bind({});
Secondary.args = {
    iconCode: '',
    cityName: 'Gdańsk',
    temp: 30,
    description: 'Słońce',
    windSpeed: 3,
    humidity: 60,
    type: WeatherTypes.Secondary,
    tempDifference: 12,
    windSpeedDifference: 5,
    humidityDifference: 40,
    windLabel: 'Prędkość wiatru: ',
    humidityLabel: 'Wilgotność: ',
};
