import React from 'react';
import { getIcon } from './utils/getIcon';
import { WeatherIcon } from './components/WeatherIcon';
import { WeatherContainer } from './styled/WeatherContainer';
import { WeatherTypes } from './constants/WeatherTypes';
import { Value } from './styled/Value';
import { units } from '../../constants/units';

interface IWeatherProps {
    iconCode: string;
    cityName: string;
    temp: number;
    description: string;
    windSpeed: number;
    humidity: number;
    type?: WeatherTypes;
    tempDifference?: number | null;
    windSpeedDifference?: number | null;
    humidityDifference?: number | null;
    windLabel: string;
    humidityLabel: string;
}

export const Weather = (
    {
        iconCode,
        cityName,
        temp,
        description,
        windSpeed,
        type = WeatherTypes.Primary,
        tempDifference = null,
        windSpeedDifference = null,
        windLabel,
        humidityDifference = null,
        humidity,
        humidityLabel,
    }: IWeatherProps): JSX.Element => {
    const roundValue = (value: number, precision: number): number => {
        const precisionValue = 10;
        const power = precisionValue ** precision;
        return Math.round(value * power) / power;
    };

    const renderDifference = (
        value: number | null,
        unit: string,
        precision: number = 1,
    ): React.ReactNode => {
        if (!value) {
            return null;
        }

        return (
            <Value isPositive={value >= 0}>
                {value > 0 ? '+ ' : ''}{roundValue(value, precision) } {unit}
            </Value>
        );
    };

    return (
        <WeatherContainer type={type}>
            <div className="weather-icon">
                <WeatherIcon path={getIcon(iconCode)}/>
            </div>
            <div className="weather-info">
                <div className="localization">{cityName}</div>
                <div className="separator"/>
                <div className="temp">
                    {Math.round(temp)} {units.DEGREE_CELSIUS}
                    {renderDifference(tempDifference, units.DEGREE_CELSIUS)}
                </div>
                <div className="secondary-info">{description}</div>
                <div className="separator"/>
                <div className="secondary-info">
                    {windLabel}{windSpeed} {units.KILOMETERS_PER_HOUR}
                    {renderDifference(windSpeedDifference, units.KILOMETERS_PER_HOUR)}
                </div>
                <div className="secondary-info">
                    {humidityLabel}{humidity} {units.PERCENT} {renderDifference(humidityDifference, units.PERCENT)}
                </div>
            </div>
        </WeatherContainer>
    );
};
