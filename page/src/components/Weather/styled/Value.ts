import styled from 'styled-components';

interface ValueProps {
    isPositive?: boolean;
    fontSize?: number;
}

export const Value = styled.div<ValueProps>`
    display: inline-block;
    font-size: 0.7em;
    padding-left: 20px;
    color: ${(props) => props.isPositive ? props.theme.colors.positive : props.theme.colors.negative};
`;
