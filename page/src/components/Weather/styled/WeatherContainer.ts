import styled from 'styled-components';
import { WeatherTypes } from '../constants/WeatherTypes';

interface WeatherContainerProps {
    type: WeatherTypes;
}

export const WeatherContainer = styled.div<WeatherContainerProps>`
    display: flex;
    align-items: center;

${(props) => props.type === WeatherTypes.Primary ? `
    color: ${props.theme.colors.basic};
    background: linear-gradient(to right bottom, #0181c2, #0181c2, #4bc4f7); 
    svg {
        fill: ${props.theme.colors.basic};
    }

    .weather-icon {
        padding: 40px;
    }
    
    .weather-info {
    
        .localization {
            text-transform: capitalize;
            font-size: ${props.theme.font.size.large}px;
        }
        
        .separator {
            border-top: 1px solid ${props.theme.colors.basic};
            opacity: 0.4;
            margin: 10px 0;
        }        
        
        .temp {
            font-size: ${props.theme.font.size.xLarge}px;
        }
        
        .secondary-info {
            color: rgb(181, 222, 244);
        }
                
        padding: 40px 40px 40px 0px;
        width: 100%;
    }
    ` : ''
}
${(props) => props.type === WeatherTypes.Secondary ? `
    color: ${props.theme.colors.secondaryLightGrey};
    background: ${props.theme.colors.basic};
    border-bottom: 1px solid ${props.theme.colors.secondaryGrey};
    border-left: 1px solid ${props.theme.colors.secondaryGrey};
    border-right: 1px solid ${props.theme.colors.secondaryGrey};
    
    svg {
        fill: ${props.theme.colors.secondaryLightGrey};
    }

    .weather-icon {
        padding: 40px;
    }
    
    .weather-info {
    
        .localization {
            text-transform: capitalize;
            font-size: ${props.theme.font.size.large}px;
        }
        
        .separator {
            border-top: 1px solid ${props.theme.colors.secondaryGrey};
            opacity: 0.4;
            margin: 10px 0;
        }        
        
        .temp {
            font-size: ${props.theme.font.size.xLarge}px;
        }
        
        .secondary-info {
            color: ${props.theme.colors.secondaryGrey};
        }
                
        padding: 40px 40px 40px 0px;
        width: 100%;
    }
    ` : ''
}
    
        .temp {
            display: flex;
            align-items: center;
        }
        
        .secondary-info {
            display: flex;
            align-items: center;
        }
    
`;
