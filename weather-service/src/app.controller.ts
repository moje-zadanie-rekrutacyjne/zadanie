import { Controller, Get, Param, Header } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';

@ApiTags('app')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @ApiResponse({
    description: 'status',
    status: 200,
  })
  getStatus(): Record<string, string> {
    return this.appService.getStatus();
  }

  @Get('/api/weather/:cityName')
  @ApiResponse({
    description: 'weather',
    status: 200,
  })
  getWeather(@Param('cityName') cityName: string) {
    return this.appService.getWeather(encodeURI(cityName));
  }
}
