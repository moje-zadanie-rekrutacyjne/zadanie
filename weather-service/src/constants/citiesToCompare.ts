export const citiesToCompare = [
  { name: 'Warszawa', id: 6695624 },
  { name: 'Gdańsk', id: 7531002 },
  { name: 'Poznań', id: 7531836 },
  { name: 'Kraków', id: 3094802 },
  { name: 'Katowice', id: 3096472 },
  { name: 'Łódź', id: 3093133 },
  { name: 'Białystok', id: 776069 },
];
