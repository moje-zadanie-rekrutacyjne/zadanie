import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import axios, { AxiosResponse } from 'axios';
import { API_KEY } from './constants/apiKey';
import { citiesToCompare as citiesList } from './constants/citiesToCompare';

@Injectable()
export class AppService {
  getStatus(): Record<string, string> {
    return {
      status: 'OK',
    };
  }

  async getWeather(cityName: string): Promise<any> {
    let weatherForCity;
    let weatherForCities;

    try {
      const { data }: AxiosResponse = await axios.get(
        `https://api.openweathermap.org/data/2.5/weather?appid=${API_KEY}&lang=pl&units=metric&q=${cityName}`,
        {},
      );
      weatherForCity = data;
    } catch (error) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          message: `city "${cityName}" not found`,
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const citiesToCompare = citiesList.filter(
      (city) => city.name !== weatherForCity.name,
    );
    const citiesId = citiesToCompare.map((city) => city.id).join(',');

    try {
      const { data }: AxiosResponse = await axios.get(
        `https://api.openweathermap.org/data/2.5/group?appid=${API_KEY}&lang=pl&units=metric&id=${citiesId}`,
        {},
      );
      weatherForCities = data;
    } catch (error) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          message: 'cities not found',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    return [weatherForCity, ...weatherForCities.list];
  }
}
