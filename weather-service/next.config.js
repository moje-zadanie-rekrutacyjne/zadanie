module.exports = {
  async rewrites() {
    return [
      {
        source: '/api/weather/:cityName*',
        destination: 'https://api.example.com/:cityName*',
      },
    ];
  },
};
